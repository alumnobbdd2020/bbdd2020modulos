DROP DATABASE IF EXISTS aplicacion3;
CREATE DATABASE aplicacion3;
USE aplicacion3;

CREATE TABLE coches(
  id int,
  marca varchar(50),
  fecha date,
  precio float,
  PRIMARY KEY(id)
);

CREATE TABLE clientes(
  cod int,
  nombre varchar(50),
  id_coche_alquilado int,
  fecha_alquiler date,
  PRIMARY KEY(cod)
);

ALTER TABLE clientes 
  ADD CONSTRAINT fkclientcoche FOREIGN KEY (id_coche_alquilado)
  REFERENCES coches(id) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT uk_clientes UNIQUE (id_coche_alquilado);