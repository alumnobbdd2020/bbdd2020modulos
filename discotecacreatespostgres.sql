drop database if exists discoteca;
create database discoteca;
create table discos(
	codigodisco serial not null,
	titulo character varying(60),
	interprete character varying(60),
	anno int,
	discografica character varying(30),
	numerocanciones int,
	nota int,
	prestado boolean,
	comentario text,
	primary key (codigodisco)
);

insert into discos(titulo,interprete,anno,discografica,numerocanciones,nota,prestado)
values  ('Estopa','Estopa',2000,'Warner',11,7,true),	
		('Escuela de Calor','Radio Futura',1980,'Sony',10,8,false),	
		('Sabor de Amor','Danza Invisible',1981,'Warner',10,7,false),
		('Perlas Ensangrentadas','Alaska',1980,'Sony',10,8,false),
		('Músico Loco','El último de la Fila',1985,'Virgin',12,6,false),
		('Soy una triste','La Oreja de Van Gaal',2002,'Dumper',11,5,true),
		('No uso champú','Milindri',2004,'Dumper',12,6,true),	
		('Pero que floja soy','El Sueño del más feo',2006,'Virgin',11,5,true),	
		('El calor del amor en un bar','Gabinete Caligari',1984,'Warner',12,6,'false'),	
		('Esta vida me va a matar','Los Suaves',1980,'Sony',10,7,false),	
		('Rojo','Barricada',1986,'Warner',11,7,false),	
		('Deltoya','Extremoduro',1990,'Virgin',12,8,false),	
		('Amor temporero','Marea',1995,'Sony',10,5,false),	
		('Si tú te vas','Platero y tú',1993,'Sony',11,9,false),	
		('Playeras','El canto del moco',2005,'Virgin',12,6,true);	

