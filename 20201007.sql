/*
  ejemplo del uso del LMD
*/
DROP DATABASE IF EXISTS ejemplo07; #Elimina la BBDD
CREATE DATABASE ejemplo07;
USE ejemplo07; -- selecciona la BBDD
CREATE TABLE personas (
  persona_id int,
  persona_nom varchar(50) NOT NULL,
  ciudad_id int
);

CREATE TABLE ciudades (
  ciudad_id int,
  ciudad_nom varchar(50) DEFAULT NULL,
  provincia varchar(50) DEFAULT 'Cantabria' NOT NULL
);

INSERT personas (persona_id, persona_nom, ciudad_id)
  VALUES (1, 'Jose', 10),
  (2, 'Ana', 9);

INSERT IGNORE ciudades (ciudad_id, ciudad_nom, provincia)
  VALUES (9, 'Santander', 'Cantabria'),
  (10, 'potes', 'Cantabria'),
  (5, 'laredo', DEFAULT),
  (11,'Reinosa',NULL);

INSERT ciudades (ciudad_id, ciudad_nom)
  VALUES (6, 'Torrelavega');

SELECT
  p.persona_id,
  p.persona_nom,
  p.ciudad_id
FROM personas p;

SELECT
  *
FROM ciudades c;