PGDMP     1    ,            	    x           bd1    13.0    13.0     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16394    bd1    DATABASE     _   CREATE DATABASE bd1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Spain.1252';
    DROP DATABASE bd1;
                postgres    false            �            1259    16414    libros    TABLE     �   CREATE TABLE public.libros (
    codigo integer NOT NULL,
    titulo character varying(30),
    autor character varying(30),
    editorial character varying(15)
);
    DROP TABLE public.libros;
       public         heap    postgres    false            �            1259    16412    libros_codigo_seq    SEQUENCE     �   CREATE SEQUENCE public.libros_codigo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.libros_codigo_seq;
       public          postgres    false    202            �           0    0    libros_codigo_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.libros_codigo_seq OWNED BY public.libros.codigo;
          public          postgres    false    201            $           2604    16417    libros codigo    DEFAULT     n   ALTER TABLE ONLY public.libros ALTER COLUMN codigo SET DEFAULT nextval('public.libros_codigo_seq'::regclass);
 <   ALTER TABLE public.libros ALTER COLUMN codigo DROP DEFAULT;
       public          postgres    false    201    202    202            �          0    16414    libros 
   TABLE DATA           B   COPY public.libros (codigo, titulo, autor, editorial) FROM stdin;
    public          postgres    false    202   �
       �           0    0    libros_codigo_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.libros_codigo_seq', 7, true);
          public          postgres    false    201            &           2606    16419    libros libros_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.libros
    ADD CONSTRAINT libros_pkey PRIMARY KEY (codigo);
 <   ALTER TABLE ONLY public.libros DROP CONSTRAINT libros_pkey;
       public            postgres    false    202            �   /   x�3�t�+���O?�6Q� ?�$39��)�(=���5759�+F��� )�     