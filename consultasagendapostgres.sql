--Mostrar nombre y apellidos de los contactos cuyo teléfono termine en 7
select nombre,apellidos,telefono
from contactos
where telefono like '%7';
--Mostrar nombre y apellidos de los contactos nacidos entre los años 1965 y 1975
select nombre,apellidos,fechanacimiento
from contactos
where fechanacimiento between '01/01/1965' and '31/12/1975';

--Mostrar todos los datos de contactos ordenados por apellido
select *
from contactos
order by apellidos;

--Mostrar nombre y apellidos de los clientes con un descuento superior al 5
select nombre,apellidos,descuento
from clientes
where descuento>5;

--Mostrar nombre, apellidos y tarjeta socio de los clientes con un descuento superior al 5 pero inferior al 15
select nombre,apellidos,tarjetasocio,descuento
from clientes
where descuento between 5 and 15;

--Mostrar nombre, apellidos y DNI de los clientes que tienen tarjeta de socio
select nombre,apellidos,dni 
from clientes
where tarjetasocio='true';

--Mostrar todos los datos de los clientes que no son socios
select *
from clientes
where tarjetasocio='false';