/* ejemplo1.
crear procedimiento almacenado que devuelva
los nombres de los ciclistas */
USE ciclistas;
DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo1$$
CREATE PROCEDURE ejemplo1 ()
BEGIN
  SELECT
    nombre
  FROM ciclista;
END$$
DELIMITER;

CALL ejemplo1();

/*
ejemplo2.
 ciclista que ha ganado mas puertos
*/

USE ciclistas;
DELIMITER $$
DROP PROCEDURE IF EXISTS ejemplo2$$
CREATE PROCEDURE ejemplo2 ()
BEGIN
  SELECT
    COUNT(*) numerodepuertos,
    p.dorsal,
    c.nombre
  FROM puerto p
    JOIN ciclista c
      ON p.dorsal = c.dorsal
  GROUP BY p.dorsal
  ORDER BY numerodepuertos DESC
  LIMIT 1;
  SELECT
    c.nombre
  FROM ciclista c
    JOIN (SELECT
        p.dorsal,
        COUNT(*) nPuertos
      FROM puerto p
      GROUP BY p.dorsal
      HAVING nPuertos = (SELECT
          MAX(c1.nPuertos)
        FROM (SELECT
            p.dorsal,
            COUNT(*) nPuertos
          FROM puerto p
          GROUP BY p.dorsal) c1)) c2
      ON c.dorsal = c2.dorsal;

END$$
DELIMITER;

CALL ejemplo2();

/*
ejemplo3
crear un procedimiento que contenga 3 variables
una=10
dos=5
tres=2
quiero mostrar la suma,resta y producto de las 3 variables 
*/

DROP PROCEDURE IF EXISTS ejemplo3;
DELIMITER //
CREATE PROCEDURE ejemplo3 ()
BEGIN
  DECLARE una int;
  DECLARE dos int;
  DECLARE tres int;
  SET una = 10;
  SET dos = 5;
  SET tres = 2;
  SELECT
    una + dos + tres AS suma,
    una - dos - tres AS resta,
    una * dos * tres AS producto;
END//

DELIMITER ;

CALL ejemplo3();

/*ejemplo4
  nombre del ciclista mas viejo
*/

DROP PROCEDURE IF EXISTS ejemplo4;
DELIMITER $$
CREATE PROCEDURE ejemplo4 ()
BEGIN
  SELECT
    c.nombre
  FROM ciclista c
  WHERE c.edad = (SELECT
      MAX(edad) edadmaxima
    FROM ciclista);
END$$
DELIMITER ;
CALL ejemplo4();

/*ejemplo5
  crear un procedimiento que le paso un numero (param) y me tiene que indicar
  la nota en texto.
  nota<5 ==> suspenso
  nota>5 ==> aprobado

  CALL ejemplo5(7) ==> APROBADO
  CALL ejemplo5(3) ==> SUSPENSO
*/

DROP PROCEDURE IF EXISTS ejemplo5;
DELIMITER $$
CREATE PROCEDURE ejemplo5 (nota int)
BEGIN

  IF (nota > 5) THEN
    SELECT
      'aprobado';
  ELSE
    SELECT
      'suspenso';
  END IF;

END$$

CALL ejemplo5(3);

/*ejemplo6
  crear un procedimiento que le paso un numero (param) y me tiene que indicar
  la nota en texto.
  nota<5 ==> suspenso
  nota>5 ==> aprobado
  nota>7 ==> sobresaliente

  CALL ejemplo5(7) ==> APROBADO
  CALL ejemplo5(3) ==> SUSPENSO
*/

DROP PROCEDURE IF EXISTS ejemplo6;
DELIMITER $$
CREATE PROCEDURE ejemplo6 (nota int)
BEGIN
  DECLARE resultado varchar(20);

  IF (nota < 5) THEN
    SET resultado = 'suspenso';
    IF (nota BETWEEN 5 AND 7) THEN
      SET resultado = 'aprobado';
    END IF;
  ELSE
    SET resultado = 'sobresaliente';
  END IF;
  SELECT
    resultado;
END$$

CALL ejemplo6(8);

/*ejemplo7
  crear un procedimiento que le paso un numero (param) y me tiene que indicar
  la nota en texto (utilizando case).
  nota<5 ==> suspenso
  nota>5 ==> aprobado
  nota>7 ==> sobresaliente

  CALL ejemplo5(7) ==> APROBADO
  CALL ejemplo5(3) ==> SUSPENSO
*/

DROP PROCEDURE IF EXISTS ejemplo7;
DELIMITER $$
CREATE PROCEDURE ejemplo7 (nota int)
BEGIN
  DECLARE resultado varchar(20);

  CASE
    WHEN (nota < 5) THEN SET resultado = 'suspenso';
    WHEN (nota < 7) THEN SET resultado = 'aprobado';
    ELSE SET resultado = 'sobresaliente';
  END CASE;
  SELECT
    resultado;
END$$
DELIMITER ;

CALL ejemplo7(9);
 

  