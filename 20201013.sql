DROP DATABASE IF EXISTS ejemplo13;
CREATE DATABASE ejemplo13;
USE ejemplo13;

#DEFINICION DE TABLAS
CREATE TABLE camionero (
  #definicion de campos
  dni char(10),
  nombre varchar(50),
  poblacion varchar(50),
  tel char(9),
  direccion varchar(50),
  salario float(8, 2) UNSIGNED,
  #definicion de indices
  PRIMARY KEY (dni)
);

CREATE TABLE camion (
  #definicion de campos
  matricula char(7),
  modelo varchar(30),
  potencia int,
  tipo varchar(20),
  #definicion de indices
  PRIMARY KEY (matricula)
);

CREATE TABLE conduce (
  #definicion de campos
  dnicamionero char(10),
  matrcamion char(7),
  #definicion de indices
  PRIMARY KEY (dnicamionero, matrcamion),
  CONSTRAINT fkcamioneroconduce FOREIGN KEY (dnicamionero)
    REFERENCES camionero(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkcamionconduce FOREIGN KEY (matrcamion)
    REFERENCES camion(matricula) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE provincia (
  #definicion de campos
  codigo int AUTO_INCREMENT,
  nombre varchar(50),
  #definicion de indices
  PRIMARY KEY (codigo)
);


CREATE TABLE paquete (
  #definicion de campos
  codigo int AUTO_INCREMENT,
  descripcion varchar(100),
  destinatario varchar(100),
  direccion varchar(80),
  codprovincia int,
  #definicion de indices
  PRIMARY KEY (codigo),
  CONSTRAINT fkpaqueteprovincia FOREIGN KEY(codprovincia)
    REFERENCES provincia(codigo) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE distribuye (
  #definicion de campos
  dnicamionero char(10),
  codpaquete int,
  #definicion de indices
  PRIMARY KEY (dnicamionero, codpaquete),
  UNIQUE uk1 (codpaquete),
  CONSTRAINT fkdistribuyecamionero FOREIGN KEY(dnicamionero)
    REFERENCES camionero(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkdistribuyepaquete FOREIGN KEY(codpaquete)
    REFERENCES paquete(codigo) ON DELETE CASCADE ON UPDATE CASCADE
                                  
);

