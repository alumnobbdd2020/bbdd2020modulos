/*
Mois�s Sarabia Ter�n
Adm�n bases de datos
pr�ctica 17 m�dulo3 unidad2
*/
-- seleccionar la base de datos para operar con ella �IMPORTANTE!
USE ciclistas;

#1.1 Numero de ciclistas que hay

SELECT COUNT(c.dorsal) AS numerociclistas
  FROM ciclista c;
  
#1.2 Numero de ciclistas que hay del equipo Banesto

SELECT COUNT(c.dorsal) AS numerociclistasdelbanesto
  FROM ciclista c
  WHERE c.nomequipo='Banesto';

#1.3 La edad media de los ciclistas

SELECT AVG(c.edad) AS mediadeedad
FROM ciclista c;

#1.4 La edad media de los de equipo Banesto

SELECT AVG(c.edad) AS madiadedaddelbanesto
FROM ciclista c
WHERE c.nomequipo='Banesto';

#1.5 La edad media de los ciclistas por cada equipo

SELECT AVG(c.edad) AS mediadeedadporequipo
FROM ciclista c
GROUP BY c.nomequipo;

#1.6 El n�mero de ciclistas por equipo

SELECT COUNT(dorsal) AS ciclistasporequipo
  FROM ciclista c
GROUP BY nomequipo;

#1.7 El n�mero total de puertos

SELECT COUNT(p.nompuerto) 
FROM puerto p;

#1.8 El n�mero total de puertos mayores de 1500

SELECT COUNT(p.nompuerto) 
FROM puerto p
WHERE p.altura>1500
GROUP BY p.nompuerto;

#1.9 Listar el nombre de los equipos que tengan m�s de 4 ciclistas

SELECT c.nomequipo,COUNT(dorsal) AS numerociclistas
FROM ciclista c
GROUP BY c.nomequipo
HAVING numerociclistas>4;

#1.10 Listar el nombre de los equipos que tengan m�s de 4 ciclistas cuya edad este entre 28 y 32

SELECT c.nomequipo,COUNT(c.dorsal) AS numerodeciclistas
FROM ciclista c
WHERE c.edad BETWEEN 28 AND 32
GROUP BY c.nomequipo
HAVING numerodeciclistas>4;

#1.11 Ind�came el n�mero de etapas que ha ganado cada uno de los ciclistas 

SELECT e.dorsal,COUNT(*) AS numerodeetapas 
FROM etapa e
GROUP BY e.dorsal;

#1.12 Ind�came el dorsal de los ciclistas que hayan ganado m�s de 1 etapa

SELECT e.dorsal,COUNT(*) AS numerodeetapas 
FROM etapa e
GROUP BY e.dorsal
HAVING numerodeetapas>1;