create table productos(
	codigoentrada serial,
	nombreproducto character varying(25),
	cantidades int,
	precio float,
	fechaentrada date,
	proveedor character varying(5),
	primary key(codigoentrada)
);

create table proveedores(
	codigoproveedor character varying(5),
	nombreproveedor character varying(25),
	domicilio character varying(25),
	paisprocedencia character varying(15),
	suscursalespaña boolean,
	primary key(codigoproveedor)
);

insert into productos (nombreproducto,cantidades,precio,fechaentrada,proveedor)
			values('COCA-COLA',175,34,'4/1/99','OLSB1'),
			      ('LECHE BRICK',8,85,'19/11/98','MILL1'),
				  ('DONUT',22,45,'20/4/98','EIDET'),
				  ('YOGHOURT',65,22,'23/11/98','MILL1'),
				  ('COCA-COLA',75,35,'3/3/99','OLSB1'),
				  ('WHISKY',6,1191,'29/12/96','JBSW1'),
				  ('TOMATE KETCHUP',25,52,'4/2/99','ORLA1');	
				  
insert into proveedores (nombreproveedor,domicilio,paisprocedencia,suscursalespaña)
		values('OLSB1','Olsberga',),