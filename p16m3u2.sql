/*
Mois�s Sarabia Ter�n
pr�ctica 16 M�dulo3 unidad2
*/

-- seleccionar la base de datos para poder operar con ella �IMPORTANTE!
USE ciclistas;

#1.1 Listar las edades de los ciclistas (sin repetidos)

SELECT DISTINCT
  c.edad
FROM ciclista c;

#1.2 Listar las edades de los ciclistas de Artiach

SELECT DISTINCT
  edad
FROM ciclista c
WHERE c.nomequipo = 'artiach';

#1.3 Listar las edades de los ciclistas de Artiach o de Amore Vita

SELECT DISTINCT
  edad
FROM ciclista c
WHERE c.nomequipo = 'artiach'
OR c.nomequipo = 'Amore Vita';

#1.4 Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30	

SELECT
  c.dorsal
FROM ciclista c
WHERE c.edad < 25
OR c.edad > 30;

#1.5 Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y adem�s que solo sean de Banesto

SELECT
  c.dorsal
FROM ciclista c
WHERE c.edad BETWEEN 28 AND 32
AND c.nomequipo = 'Banesto';

#1.6 Ind�came el nombre de los ciclistas que el n�mero de caracteres del nombre sea mayor que 8

SELECT
  c.nombre
FROM ciclista c
WHERE CHAR_LENGTH(c.nombre) > 8;

#1.7 L�stame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre may�sculas que debe mostrar el nombre en may�sculas

SELECT
  UPPER(nombre) AS nombremayuscula,
  dorsal
FROM ciclista;

#1.8 Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa	

SELECT
  dorsal
FROM lleva
WHERE c�digo = 'MGE';

#1.9 Listar el nombre de los puertos cuya altura sea mayor que 1500

SELECT
  p.nompuerto
FROM puerto p
WHERE p.pendiente > 8
OR p.altura BETWEEN 1800 AND 3000;

#1.10  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000	8 
SELECT
  p.dorsal
FROM puerto p
WHERE p.pendiente > 8
OR p.altura BETWEEN 1800 AND 3000; 
