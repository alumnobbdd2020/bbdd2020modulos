#1.1 Nombre y edad de los ciclistas que han ganado etapas

-- consulta directa
SELECT DISTINCT c.nombre,c.edad 
FROM ciclista c
JOIN etapa e ON c.dorsal = e.dorsal;

-- consulta optimizada
-- c1
SELECT DISTINCT e.dorsal FROM etapa e;

SELECT c.nombre,c.edad FROM ciclista c
JOIN (SELECT DISTINCT e.dorsal FROM etapa e)c1
ON c1.dorsal=c.dorsal;

#1.2 Nombre y edad de los ciclistas que han ganado puertos

-- c1
SELECT DISTINCT p.dorsal FROM puerto p;

SELECT c.nombre,c.edad FROM ciclista c
  JOIN (SELECT DISTINCT p.dorsal FROM puerto p)c1
  ON c1.dorsal=c.dorsal;

#1.3 Nombre y edad de los ciclistas que han ganado etapas y puertos

SELECT DISTINCT p.dorsal FROM puerto p;

SELECT c.nombre,c.edad FROM ciclista c
  JOIN (SELECT DISTINCT p.dorsal,p.numetapa FROM puerto p)c1
  ON c1.dorsal=c.dorsal;

#1.4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa

SELECT DISTINCT e.director 
  FROM equipo e
  JOIN ciclista c ON e.nomequipo = c.nomequipo
  JOIN etapa e1 ON c.dorsal = e1.dorsal;


#1.5 Dorsal y nombre de los ciclistas que hayan llevado alg�n maillot

SELECT DISTINCT c.dorsal, c.nombre 
  FROM ciclista c
JOIN lleva l ON c.dorsal = l.dorsal
WHERE l.c�digo IS NOT NULL;

#1.6 Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo 

SELECT DISTINCT c.dorsal, c.nombre 
  FROM ciclista c
JOIN lleva l ON c.dorsal = l.dorsal
JOIN maillot m ON l.c�digo = m.c�digo
WHERE l.c�digo='MGE';

#1.7 Dorsal de los ciclistas que hayan llevado alg�n maillot y que han ganado etapas
  
SELECT DISTINCT l.dorsal 
  FROM lleva l
  WHERE l.numetapa AND l.c�digo IS NOT NULL;
   
#1.8 Indicar el numetapa de las etapas que tengan puerto

SELECT e.numetapa 
  FROM etapa e
  JOIN puerto p ON e.numetapa = p.numetapa
  WHERE p.nompuerto IS NOT NULL;

#1.9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos

SELECT e.kms 
  FROM etapa e
  JOIN ciclista c ON e.dorsal = c.dorsal
  JOIN puerto p ON c.dorsal = p.dorsal
  WHERE c.nomequipo='Banesto' AND p.nompuerto IS NOT NULL;

#1.10 Listar el n�mero de ciclistas que hayan ganado alguna etapa con puerto 

SELECT COUNT(c.dorsal)
  FROM ciclista c 
  JOIN puerto p ON c.dorsal = p.dorsal
  WHERE p.nompuerto IS NOT NULL;

#1.11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto 

SELECT p.nompuerto 
  FROM puerto p

#1.12 Listar el n�mero de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 km 6