truncate table libros;
drop table if exists libros;
create table libros(
 codigo serial,
 titulo varchar(30),
 autor varchar(30),
 editorial varchar(15),
 primary key (codigo)
);

insert into libros (titulo,autor,editorial)
 values('Martin Fierro','Jose Hernandez','Planeta');
insert into libros (titulo,autor,editorial)
 values('Aprenda PHP','Mario Molina','Emece');
insert into libros (titulo,autor,editorial)
 values('Cervantes y el quijote','Borges','Paidos');
insert into libros (titulo,autor,editorial)
 values('Matematica estas ahi', 'Paenza', 'Paidos');
insert into libros (titulo,autor,editorial)
 values('El aleph', 'Borges', 'Emece');

-- Eliminemos todos los registros con "delete":
delete from libros;
-- Veamos el resultado:
select * from libros;
-- La tabla ya no contiene registros.

-- Ingresamos un nuevo registro:
insert into libros (titulo,autor,editorial)
 values('Antología poetica', 'Borges', 'Emece');

-- Ahora vaciemos la tabla:
truncate table libros;

