/*
Mois�s Sarabia Ter�n
Adm�n de bases de datos
Ejemplo del 11 nov 2020  
*/

-- eliminar la BBDD si existe para crearla de nuevo 
DROP DATABASE IF EXISTS ejemplo20201109;
CREATE DATABASE ejemplo20201109;

-- seleccionar la BBDD para operar con ella �IMPORTANTE!
USE ejemplo20201109;

-- creacion de tablas con claves primarias
CREATE TABLE clientes (
  codcli int AUTO_INCREMENT,
  nombre varchar(20),
  apellidos varchar(50) NOT NULL,
  fechanac date,
  poblacion int NOT NULL,
  PRIMARY KEY (codcli)
);

CREATE TABLE poblacion (
  idpob int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY (idpob)
);

CREATE TABLE productos (
  codpro int AUTO_INCREMENT,
  nombre varchar(50),
  precio float,
  PRIMARY KEY (codpro)
);

CREATE TABLE compran (
  idcompran int AUTO_INCREMENT,
  codcli int NOT NULL,
  codpro int NOT NULL,
  PRIMARY KEY (idcompran)
);

CREATE TABLE compranp (
  idcompranp int,
  fecha date,
  cantidad int,
  total float,
  PRIMARY KEY (idcompranp, fecha)
);

-- modificar las tablas para insertar las claves ajenas 
ALTER TABLE clientes
ADD CONSTRAINT fkclientespoblacion FOREIGN KEY (poblacion)
REFERENCES poblacion (idpob) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE compran
ADD CONSTRAINT fkcompranclientes FOREIGN KEY (codcli)
REFERENCES clientes (codcli) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT fkcompranproductos FOREIGN KEY (codpro)
REFERENCES productos (codpro) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT ukcompran UNIQUE KEY (codcli, codpro);

ALTER TABLE compranp
ADD CONSTRAINT fkcomprancompran FOREIGN KEY (idcompranp)
REFERENCES compran (idcompran) ON DELETE CASCADE ON UPDATE CASCADE;

