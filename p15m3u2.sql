DROP DATABASE IF EXISTS p15m3u2;
CREATE DATABASE p15m3u2;
USE p15m3u2;

CREATE TABLE federacion(
  nombre varchar(50),
  direccion varchar(80),
  telefono char(9),
  PRIMARY KEY(nombre)
);

CREATE TABLE miembro(
  dni char(10),
  nombre_m varchar(50),
  titulacion varchar(40),
  PRIMARY KEY(dni)
);

CREATE TABLE composicion(
  nombre varchar(50),
  dni char(10),
  cargo varchar(30),
  fechainicio date,
  PRIMARY KEY(nombre,dni)
);

#ejercicio2
ALTER TABLE composicion
  ADD COLUMN clave int AUTO_INCREMENT,
  DROP PRIMARY KEY,
  ADD CONSTRAINT PRIMARY KEY (clave),
  ADD CONSTRAINT uk1 UNIQUE KEY(nombre,dni),
  ADD CONSTRAINT fkcompodni FOREIGN KEY(dni)
    REFERENCES miembro(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT fkcomponombre FOREIGN KEY(nombre)
    REFERENCES federacion(nombre) ON DELETE CASCADE ON UPDATE CASCADE;

#ejercicio3

  INSERT federacion (nombre, direccion, telefono)
  VALUES ('ciclismo', 'calle abajo 46', '942111111'),
         ('futbol','calle arriba 99','942222222');

  INSERT miembro (dni, nombre_m, titulacion)
  VALUES ('111111111A', 'Ramon', 'CFGM mecanica de vehiculos'),
         ('222222222C','Pedro','Diplomado INEF');

  INSERT composicion (nombre, dni, cargo, fechainicio)
  VALUES ('Ciclismo', '111111111A', 'mecanico bicicletas','1970/03/03'),
         ('Futbol','222222222C','preparador fisico','1985/05/07');
          