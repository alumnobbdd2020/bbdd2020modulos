
+-/*
Mois�s Sarabia Ter�n
Adm�n de bases de datos
Practica 18 m�dulo3 Unidad2
*/

#1.1 Listar las edades de todos los ciclistas de Banesto.

SELECT
  edad
FROM ciclista
WHERE nomequipo = 'banesto';

#1.2 Listar las edades de los ciclistas que son de Banesto o de Navigare.

SELECT
  edad
FROM ciclista c
WHERE c.nomequipo = 'banesto'
OR c.nomequipo = 'navigare';

#1.3 Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32.

SELECT
  c.dorsal
FROM ciclista c
WHERE c.nomequipo = 'Banesto'
AND c.edad BETWEEN 25 AND 32;


#1.4 Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32.

SELECT
  c.dorsal
FROM ciclista c
WHERE c.nomequipo = 'Banesto'
OR c.edad BETWEEN 25 AND 32;

#1.5 Listar la inicial del equipo de los ciclistas cuyo nombre comience por R 

SELECT DISTINCT LEFT(c.nomequipo,1)
  FROM ciclista c
  WHERE LEFT(C.nombre,1)='R';


#1.6 Listar el c�digo de las etapas que su salida y llegada sea en la misma poblaci�n.
  
 SELECT e.numetapa 
  FROM etapa e
  WHERE e.salida=e.llegada;
  
#1.7 Listar el c�digo de las etapas que su salida y llegada no sean en la misma poblaci�n y que conozcamos el dorsal del ciclista que ha ganado la etapa.

SELECT e.numetapa 
  FROM etapa e
  WHERE e.salida NOT LIKE e.llegada AND e.dorsal IS NOT NULL;

#1.8 Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400.

SELECT p.nompuerto 
  FROM puerto p
  WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;

#1.9 Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400.

SELECT DISTINCT p.dorsal
  FROM puerto p
  WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;

#1.10 Listar el n�mero de ciclistas que hayan ganado alguna etapa

SELECT COUNT(e.dorsal) AS numerodeciclistas
FROM etapa e;


#1.11 Listar el n�mero de etapas que tengan puerto 

 SELECT COUNT(numetapa) AS numeroetapas
 FROM puerto p;
    
#1.12 Listar el n�mero de ciclistas que hayan ganado alg�n puerto
  
SELECT COUNT(dorsal) AS numerodeciclistas
FROM puerto p; 
   
#1.13 Listar el c�digo de la etapa con el n�mero de puertos que tiene.

SELECT p.numetapa,COUNT(p.nompuerto) 
  FROM puerto p
  GROUP BY p.numetapa;

#1.14 Indicar la altura media de los puertos

SELECT AVG(altura) promediodealtura
  FROM puerto p;


#1.15 Indicar el c�digo de etapa cuya altura media de sus puertos est� por encima de 1500. 

SELECT p.numetapa,AVG(p.altura) AS alturamedia
  FROM puerto p
  GROUP BY p.numetapa
  HAVING alturamedia>1500;

#1.16 Indicar el n�mero de etapas que cumplen la condici�n anterior.

SELECT COUNT(p.numetapa) numerodeetapas,AVG(p.altura) AS alturamedia
  FROM puerto p
  HAVING alturamedia>1500;

#1.17 Listar el dorsal del ciclista con el n�mero de veces que ha llevado alg�n maillot. 

SELECT dorsal,COUNT(c�digo) numerodemaillots
FROM lleva
GROUP BY dorsal;

#1.18 Listar el dorsal del ciclista con el c�digo de maillot y cuantas veces ese ciclista ha llevado ese maillot.

SELECT l.dorsal,l.c�digo,COUNT(l.c�digo) AS veces 
  FROM lleva l
  GROUP BY l.c�digo;



