-- Mostrar título e Intérprete de todos los discos.
select titulo,interprete
from discos;
--Mostrar título, intérprete, año y discográfica de todos los discos que tengan más de 10 canciones.
select titulo,interprete,anno,discografica
from discos
where numerocanciones>10;
--Mostrar título de todos los discos que tengan 10 canciones.
select titulo
from discos
where numerocanciones=10;
--Mostrar los intérpretes cuya discográfica sea Warner.
select interprete
from  discos
where discografica='Warner';
--Mostrar los títulos, el año, la nota y la discográfica de todos los discos prestados.
select titulo,anno,nota,discografica
from discos
where prestado=true;
--Mostrar el título, intérprete y número de canciones de los discos de la década de los 80.
select titulo,interprete,numerocanciones
from discos
where anno between 1980 and 1989;
--Mostrar el título e intérprete de los discos con nota mayor o igual que 8.
select titulo,interprete
from discos
where nota>=8;
--Mostrar los intérpretes de todos los discos que contengan la palabra amor.
select interprete
from discos
where titulo like '%amor%';
--Mostrar el código del disco, el título y el intérprete de los discos que pertenezcan a la discográfica Warner y tengan 11 canciones.
select codigodisco,titulo,interprete
from discos 
where discografica='Warner' and numerocanciones=11;