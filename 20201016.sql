#eliminar la BBDD (si existe) y crear la BBDD vac�a
DROP DATABASE IF EXISTS ejemplo16;
CREATE DATABASE ejemplo16;
USE ejemplo16;

#definir tablas
CREATE TABLE alumno(
  #definicion de campos
  expediente int AUTO_INCREMENT NOT NULL,
  nombre varchar(30),
  apellidos varchar(50),
  fecha_nac date,
  #definicion de restricciones
  PRIMARY KEY (expediente) -- clave primaria
);

CREATE TABLE modulo(
  #definicion de campos
  codigo int AUTO_INCREMENT NOT NULL,
  nombre varchar(50),
  #definicion de restricciones
  PRIMARY KEY (codigo) -- clave primaria
);

CREATE TABLE profesor(
  #definicion de campos
  dni char(10) NOT NULL,
  nombre varchar(30),
  direccion varchar(50),
  telefono char(9),
  #definicion de restricciones
  PRIMARY KEY(dni) -- clave primaria
);

CREATE TABLE esdelegado(
  #definicion de campos
  expedientedelegado int AUTO_INCREMENT NOT NULL,
  expedientealumno int NOT NULL,
  #definicion de restricciones
  PRIMARY KEY (expedientedelegado,expedientealumno), -- clave primaria
  CONSTRAINT fkdelegadoalumno FOREIGN KEY(expedientedelegado)
    REFERENCES esdelegado(expedientealumno), -- clave ajena
  CONSTRAINT fkdelegadoalumno2 FOREIGN KEY(expedientedelegado)
    REFERENCES esdelegado(expedientedelegado) ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE KEY uk1(expedientealumno) -- indice sin duplicados
);

CREATE TABLE cursa(
  #definicion de campos
  expedientealumno int NOT NULL,
  codigomodulo int NOT NULL,
  #definicion de restricciones
  PRIMARY KEY(expedientealumno,codigomodulo), -- clave primaria
  CONSTRAINT fkcursaalumno FOREIGN KEY(expedientealumno)
    REFERENCES alumno(expediente) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkcursamodulo FOREIGN KEY(codigomodulo) 
    REFERENCES modulo(codigo) ON DELETE CASCADE ON UPDATE CASCADE -- claves ajenas con restriccion de borrado y actualizacion en cascada
);

CREATE TABLE imparte(
  #definicion de campos
  dniprofesor char(10) NOT NULL,
  codigomodulo int NOT NULL,
  #definicion de restricciones
  PRIMARY KEY(dniprofesor,codigomodulo), -- clave primaria
  CONSTRAINT fkimparteprofesor FOREIGN KEY (dniprofesor)
    REFERENCES profesor(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkimpartemodulo FOREIGN KEY(codigomodulo)
    REFERENCES modulo(codigo) ON DELETE CASCADE ON UPDATE CASCADE,-- claves ajenas con restriccion de borrado y actualizacion en cascada  
  UNIQUE KEY uk2(codigomodulo) -- indice sin duplicados
);