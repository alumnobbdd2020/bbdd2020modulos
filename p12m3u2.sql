/*
Mois�s Sarabia Ter�n 
Admon. de Bases de datos
Pr�ctica 12 M�dulo3 Unidad2
*/

#Ejercicio2
DROP DATABASE IF EXISTS p12m3u2;
CREATE DATABASE p12m3u2;
USE p12m3u2;

#Ejercicio3
CREATE TABLE alumno(
  matricula int AUTO_INCREMENT NOT NULL, 
  nombre varchar(30),
  grupo char(2),
  PRIMARY KEY(matricula)
);

CREATE TABLE practica(
  numero int AUTO_INCREMENT NOT NULL,
  titulo varchar(50),
  dificultad varchar(20),
  PRIMARY KEY(numero)
);

CREATE TABLE realiza(
  matriculaalumno int NOT NULL,
  numeropractica int NOT NULL,
  PRIMARY KEY(matriculaalumno,numeropractica),
  CONSTRAINT fkrealizaalumno FOREIGN KEY(matriculaalumno)
    REFERENCES alumno(matricula) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkrealizapractica FOREIGN KEY(numeropractica)
    REFERENCES practica(numero) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE fechap(
  matriculaalumno int NOT NULL,
  numeropractica int NOT NULL,
  nota int,
  fecha date,
  PRIMARY KEY(matriculaalumno,numeropractica),
  CONSTRAINT fkfechaprealiza FOREIGN KEY(matriculaalumno,numeropractica)
    REFERENCES realiza(matriculaalumno,numeropractica) ON DELETE CASCADE ON UPDATE CASCADE
  
);