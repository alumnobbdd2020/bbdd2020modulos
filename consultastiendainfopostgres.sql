﻿-- 1.1.4 Consultas multitabla (Composición interna)
--1.	Devuelve un listado con todos los productos de los fabricantes Asus, Hewlett-Packard y Seagate. Sin utilizar el operador IN.

SELECT
  producto.nombre,
  fabricante.nombre
FROM producto
  JOIN fabricante
    ON producto.codigo_fabricante = fabricante.codigo
WHERE fabricante.nombre = 'Asus'
OR fabricante.nombre = 'Hewlett-Packard'
OR fabricante.nombre = 'Seagate';

--2.	Devuelve un listado con todos los productos de los fabricantes Asus, Hewlett-Packardy Seagate. Utilizando el operador IN.

SELECT
  producto.nombre,
  fabricante.nombre
FROM producto
  JOIN fabricante
    ON producto.codigo_fabricante = fabricante.codigo
WHERE fabricante.nombre = 'Asus'
OR fabricante.nombre = 'Hewlett-Packard'
OR fabricante.nombre = 'Seagate';

--3.	Devuelve un listado con el nombre y el precio de todos los productos de los fabricantes cuyo nombre termine por la vocal e.

SELECT
  producto.nombre,
  producto.precio
FROM producto
  JOIN fabricante
    ON producto.codigo_fabricante = fabricante.codigo
WHERE fabricante.nombre LIKE '%e';

--4.	Devuelve un listado con el nombre y el precio de todos los productos cuyo nombre de fabricante contenga el carácter w en su nombre.

SELECT
  producto.nombre,
  producto.precio
FROM producto
  JOIN fabricante
    ON producto.codigo_fabricante = fabricante.codigo
WHERE fabricante.nombre LIKE '%w%';

--5.	Devuelve un listado con el nombre de producto, precio y nombre de fabricante, de todos los productos que tengan un precio mayor o igual a 180€. Ordene el resultado en primer lugar por el precio (en orden descendente) y en segundo lugar por el nombre (en orden ascendente).

SELECT
  producto.nombre,
  producto.precio,
  fabricante.nombre
FROM producto
  JOIN fabricante
    ON producto.codigo_fabricante = fabricante.codigo
WHERE producto.precio > 180
ORDER BY producto.precio DESC, producto.precio ASC;


--1.1.5 Consultas multitabla (Composición externa)
--Resuelva todas las consultas utilizando las cláusulas LEFT JOIN y RIGHT JOIN.
--1.	Devuelve un listado de todos los fabricantes que existen en la base de datos, junto con los productos que tiene cada uno de ellos. El listado deberá mostrar también aquellos fabricantes que no tienen productos asociados.

SELECT
  fabricante.nombre,
  producto.nombre
FROM fabricante
  LEFT OUTER JOIN producto
    ON producto.codigo_fabricante = fabricante.codigo;

--2.	Devuelve un listado donde sólo aparezcan aquellos fabricantes que no tienen ningún producto asociado.

SELECT
  fabricante.nombre,
  producto.nombre
FROM fabricante
  LEFT OUTER JOIN producto
    ON producto.codigo_fabricante = fabricante.codigo
WHERE producto.nombre IS NULL;

--1.1.6 Consultas resumen
--1.	Calcula el número total de productos que hay en la tabla productos.

SELECT
  COUNT(producto.codigo) AS numerodeproductos
FROM producto;

--2.	Calcula el número de valores distintos de código de fabricante aparecen en la tabla productos.

SELECT
  COUNT(codigo_fabricante)
FROM producto
  JOIN fabricante
    ON fabricante.codigo = producto.codigo_fabricante
GROUP BY producto.codigo;

--3.	Calcula el precio más barato de todos los productos.

SELECT
  MIN(producto.precio) AS preciomasbajo
FROM producto;

--4.	Calcula el precio más caro de todos los productos.

SELECT
  MAX(producto.precio) AS preciomasalto
FROM producto;

--5.	Lista el nombre y el precio del producto más caro.

SELECT
  producto.nombre,
  producto.precio
FROM producto
ORDER BY producto.precio DESC LIMIT 1;

--6.	Calcula la suma de los precios de todos los productos.

SELECT
  SUM(producto.precio) sumaprecioproductos
FROM producto;

--7.	Calcula el precio más barato de todos los productos del fabricante Asus.

SELECT
  MAX(producto.precio) AS preciomasalto
FROM producto
  JOIN fabricante
    ON fabricante.codigo = producto.codigo_fabricante
WHERE fabricante.nombre = 'Asus';

--8.	Muestra el precio máximo, precio mínimo, precio medio y el número total de productos que tiene el fabricante Crucial.

SELECT
  MAX(producto.precio) preciomaximo,
  MIN(producto.precio) preciominimo,
  AVG(producto.precio),
  COUNT(producto.codigo) numerodeproductos
FROM producto
  JOIN fabricante
    ON fabricante.codigo = producto.codigo_fabricante
WHERE fabricante.nombre = 'Crucial';

--9.	Muestra el precio máximo, precio mínimo y precio medio de los productos de cada uno de los fabricantes. El resultado mostrará el nombre del fabricante junto con los datos que se solicitan.

SELECT
  fabricante.nombre,
  MAX(producto.precio) preciomaximo,
  MIN(producto.precio) preciominimo,
  AVG(producto.precio),
  COUNT(producto.codigo) numerodeproductos
FROM producto
  JOIN fabricante
    ON fabricante.codigo = producto.codigo_fabricante
GROUP BY fabricante.nombre;

--10.	Muestra el precio máximo, precio mínimo, precio medio y el número total de productos de los fabricantes que tienen un precio medio superior a 200€. No es necesario mostrar el nombre del fabricante, con el código del fabricante es suficiente.

SELECT
  producto.codigo_fabricante,
  MAX(producto.precio) preciomaximo,
  MIN(producto.precio) preciominimo,
  AVG(producto.precio) preciomedio,
  COUNT(producto.codigo) numerodeproductos
FROM producto
GROUP BY producto.codigo
HAVING AVG(producto.precio) > 200;

--11.	Muestra el nombre de cada fabricante, junto con el precio máximo, precio mínimo, precio medio y el número total de productos de los fabricantes que tienen un precio medio superior a 200€. Es necesario mostrar el nombre del fabricante.
SELECT
  fabricante.nombre,
  MAX(producto.precio) preciomaximo,
  MIN(producto.precio) preciominimo,
  AVG(producto.precio) preciomedio,
  COUNT(producto.codigo) numerodeproductos
FROM producto
  JOIN fabricante
    ON fabricante.codigo = producto.codigo_fabricante
GROUP BY fabricante.nombre
HAVING AVG(producto.precio) > 200;

--12.	Calcula el número de productos que tiene cada fabricante con un precio mayor o igual a 180€.

SELECT COUNT(codigo_fabricante)productosporfabricante,producto.codigo_fabricante
FROM producto
WHERE producto.precio>180
GROUP BY producto.codigo_fabricante;

--13.	Lista los nombres de los fabricantes cuyos productos tienen un precio medio mayor o igual a 150€.

SELECT fabricante.nombre
  FROM fabricante
  JOIN producto ON producto.codigo_fabricante=fabricante.codigo
  GROUP BY fabricante.nombre
  HAVING AVG(producto.precio)>150;


--14.	Devuelve un listado con los nombres de los fabricantes que tienen 2 o más productos.

SELECT fabricante.nombre
  FROM fabricante
  JOIN producto ON producto.codigo_fabricante=fabricante.codigo
  GROUP BY fabricante.nombre
  HAVING COUNT(producto.codigo)>2;

--15.	Devuelve un listado con los nombres de los fabricantes y el número de productos que tiene cada uno con un precio superior o igual a 220 €. El listado debe mostrar el nombre de todos los fabricantes, es decir, si hay algún fabricante que no tiene productos con un precio superior o igual a 220€ deberá aparecer en el listado con un valor igual a 0 en el número de productos.

SELECT fabricante.nombre,COUNT(producto.codigo)numerodeproductos
  FROM fabricante
  RIGHT OUTER JOIN producto ON producto.codigo_fabricante=fabricante.codigo
WHERE producto.precio>220
GROUP BY fabricante.nombre;

--16.	Devuelve un listado con los nombres de los fabricantes donde la suma del precio de todos sus productos es superior a 1000 €.

SELECT fabricante.nombre
  FROM fabricante
  JOIN producto ON producto.codigo_fabricante=fabricante.codigo
  GROUP BY fabricante.nombre
  HAVING SUM(producto.precio)>1000;

--1.1.7 Subconsultas (En la cláusula WHERE)
--1.1.7.1 Con operadores básicos de comparación
--1.	Devuelve todos los productos del fabricante Lenovo. (Sin utilizar INNER JOIN).

SELECT producto.nombre
FROM producto
WHERE codigo_fabricante = (
  SELECT codigo
  FROM fabricante
  WHERE nombre = 'Lenovo');

--2.	Devuelve todos los datos de los productos que tienen el mismo precio que el producto más caro del fabricante Lenovo. (Sin utilizar INNER JOIN).

SELECT *
FROM producto
WHERE codigo_fabricante = (
  SELECT codigo
  FROM fabricante
  WHERE nombre = 'Lenovo')
  order by producto.precio
  limit 1;

--3.	Lista el nombre del producto más caro del fabricante Lenovo.

SELECT producto.nombre
FROM producto
WHERE codigo_fabricante = (
  SELECT codigo
  FROM fabricante
  WHERE nombre = 'Lenovo')
  order by producto.precio
  limit 1;

--4.	Lista el nombre del producto más barato del fabricante Hewlett-Packard.

SELECT producto.nombre
FROM producto
WHERE codigo_fabricante = (
  SELECT codigo
  FROM fabricante
  WHERE nombre = 'Hewlett-Packard')
  order by producto.precio desc
  limit 1;


--5.	Devuelve todos los productos de la base de datos que tienen un precio mayor o igual al producto más caro del fabricante Lenovo.

SELECT *
FROM producto 
WHERE producto.precio IN(
  SELECT precio
  FROM producto,fabricante
  WHERE fabricante.codigo=producto.codigo_fabricante
    AND fabricante.nombre= 'Lenovo'
  order by producto.precio ASC);

--6.	Lista todos los productos del fabricante Asus que tienen un precio superior al precio medio de todos sus productos.
--1.1.7.2 Subconsultas con ALL y ANY
--1.	Devuelve el producto más caro que existe en la tabla producto sin hacer uso de MAX, ORDER BY ni LIMIT.
SELECT *
FROM producto
WHERE producto.precio>= ALL(
  SELECT precio
  FROM producto
);
--2.	Devuelve el producto más barato que existe en la tabla producto sin hacer uso de MIN, ORDER BY ni LIMIT.

SELECT *
FROM producto
WHERE producto.precio<= ALL(
  SELECT precio
  FROM producto
);


--1.1.7.3 Subconsultas con IN y NOT IN
--1.	Devuelve los nombres de los fabricantes que tienen productos asociados(Utilizando IN o NOT IN).

SELECT f.nombre FROM fabricante f WHERE f.codigo IN(
SELECT p.codigo FROM producto p);

--1.1.7.4 Subconsultas con EXISTS y NOT EXISTS
--1.	Devuelve los nombres de los fabricantes que tienen productos asociados. (Utilizando EXISTS o NOT EXISTS).

SELECT f.nombre FROM fabricante f WHERE EXISTS(
SELECT p.codigo FROM producto p);

--2.	Devuelve los nombres de los fabricantes que no tienen productos asociados. (Utilizando EXISTS o NOT EXISTS).

SELECT f.nombre FROM fabricante f 
LEFT JOIN (SELECT f.nombre, p.codigo FROM fabricante f 
JOIN producto p ON f.codigo = p.codigo_fabricante WHERE EXISTS(
SELECT p.codigo FROM producto p))c1 ON c1.nombre= f.nombre 
WHERE c1.codigo IS NULL;


--1.1.7.5 Subconsultas correlacionadas
--1.	Lista el nombre de cada fabricante con el nombre y el precio de su producto más caro.

SELECT p.nombre,f.nombre,p.precio FROM producto p 
 INNER JOIN fabricante f ON p.codigo_fabricante=f.codigo 
  WHERE p.precio >=(
    SELECT max(p.precio) FROM producto p
    WHERE p.codigo_fabricante = f.codigo);


--2.	Devuelve un listado de todos los productos que tienen un precio mayor o igual a la media de todos los productos de su mismo fabricante.

SELECT * FROM producto p WHERE p.precio >=
(SELECT AVG(p2.precio) FROM producto p2
  WHERE p2.codigo_fabricante = p.codigo_fabricante 
);


--3.	Lista el nombre del producto más caro del fabricante Lenovo.

SELECT p.nombre FROM producto p WHERE p.precio=(
SELECT MAX( p.precio) FROM producto p WHERE p.codigo_fabricante IN (
SELECT f.codigo FROM fabricante f WHERE f.nombre='Lenovo')
);
