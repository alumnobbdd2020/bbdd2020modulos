﻿DROP DATABASE IF EXISTS ejemplo13;
CREATE DATABASE ejemplo13;
USE ejemplo13;

DROP TABLE IF EXISTS camionero;
CREATE TABLE camionero(
  #Definicion de campos
  dni char(10),
  nombre varchar(50),
  poblacion varchar(50),
  tfno char(15),
  direccion varchar(100),
  salario float(8,2) UNSIGNED,

  #Definir las claves
  PRIMARY KEY(dni)
);

DROP TABLE IF EXISTS camion;
CREATE TABLE camion(
  matricula char(8),
  modelo varchar(50),
  potencia int(5) ZEROFILL UNSIGNED,
  tipo varchar(20),

  PRIMARY KEY(matricula)

);

DROP TABLE IF EXISTS conduce;
CREATE TABLE conduce(
  dniCamionero char(10),
  matCamion char(8),

  PRIMARY KEY(dniCamionero,matCamion)
);


DROP TABLE IF EXISTS paquete;
CREATE TABLE paquete(
  codigo int AUTO_INCREMENT,
  descripcion varchar(100),
  destinatario varchar(50),
  direccion varchar(100),
  codProvincia int,

  PRIMARY KEY(codigo)
);

DROP TABLE IF EXISTS distribuye;
CREATE TABLE distribuye(
  dniCamionero char(10),
  codPaquete int,

  PRIMARY KEY(dniCamionero,codPaquete),
  UNIQUE uk1(codPaquete)
);

DROP TABLE IF EXISTS provincia;
CREATE TABLE provincia(
  codigo int AUTO_INCREMENT,
  nombre varchar(50),

  PRIMARY KEY(codigo)
);







