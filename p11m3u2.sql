/*
  Mois�s Sarabia Ter�n
  Pr�ctica11 m�dulo3 unidad2
*/

#ejercicio2
DROP DATABASE IF EXISTS p11m3u2;
CREATE DATABASE p11m3u2;
USE p11m3u2;

#ejercicio3
CREATE TABLE alumnos(
  matricula int AUTO_INCREMENT NOT NULL,
  nombre varchar(50),
  grupo char(2),
  PRIMARY KEY(matricula)
);

CREATE TABLE practica(
  numero int AUTO_INCREMENT NOT NULL,
  titulo varchar(80),
  dificultad varchar(20),
  PRIMARY KEY(numero)
);

CREATE TABLE realiza(
  matriculaalumno int NOT NULL,
  numeropractica int NOT NULL,
  fecha date,
  nota int,
  PRIMARY KEY(matriculaalumno,numeropractica),
  CONSTRAINT fkrealizaalumno FOREIGN KEY(matriculaalumno)
    REFERENCES alumnos(matricula) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkrealizapractica FOREIGN KEY(numeropractica)
    REFERENCES practica(numero) ON DELETE CASCADE ON UPDATE CASCADE
);