/*
Mois�s Sarabia Ter�n
Adm�n de bases de datos
Practica13 m�dulo3 unidad2
*/

#ejercicio1
DROP DATABASE IF EXISTS p13m3u2;
CREATE DATABASE p13m3u2;
USE p13m3u2;

CREATE TABLE pacientes(
  codigopaciente int AUTO_INCREMENT NOT NULL,
  nombre varchar(20),
  apellidos varchar(80),
  PRIMARY KEY (codigopaciente)
);

CREATE TABLE medico(
  codigomedico int AUTO_INCREMENT NOT NULL,
  nombre varchar(50),
  PRIMARY KEY(codigomedico)
);

CREATE TABLE medicamentos(
  codigomedicamento varchar(15) NOT NULL,
  nombre varchar(100),
  PRIMARY KEY(codigomedicamento)
);

CREATE TABLE consultas(
  codigoconsulta varchar(15) NOT NULL,
  fechaconsulta date,
  codigomedico int,
  codigopaciente int,
  PRIMARY KEY(codigoconsulta)
);

CREATE TABLE consultamedicamentos(
  codigoconsulta varchar(15) NOT NULL,
  codigomedicamento varchar(15) NOT NULL,
  dosis varchar(15),
  PRIMARY KEY(codigoconsulta,codigomedicamento)
);

#Ejercicio2

ALTER TABLE consultas
  ADD CONSTRAINT fkconsultaspacientes FOREIGN KEY(codigopaciente)
    REFERENCES pacientes(codigopaciente) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE consultamedicamentos
  ADD CONSTRAINT fkconsultasmedicamentoscons FOREIGN KEY(codigoconsulta)
    REFERENCES consultas(codigoconsulta) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE consultamedicamentos
  ADD CONSTRAINT fkconsultasmedicamentosmed FOREIGN KEY(codigomedicamento)
    REFERENCES medicamentos(codigomedicamento) ON DELETE CASCADE ON UPDATE CASCADE;