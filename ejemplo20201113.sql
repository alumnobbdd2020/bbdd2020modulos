USE ejemplo20201109;
#1- Nombre del cliente junto con el nombre del producto que han comprado

SELECT c.nombre nombrecliente,p.nombre nombreproducto
FROM clientes c
  JOIN compran c1 ON c.codcli = c1.codcli
  JOIN productos p ON c1.codpro = p.codpro;

#2- Nombre de la poblacion donde vive el cliente , nombre del producto que ha comprado y nombre del cliente

SELECT p.nombre nombrepoblacion, p1.nombre nombreproducto
FROM poblacion p
  JOIN clientes c ON p.idpob = c.poblacion
  JOIN compran c1 ON c.codcli = c1.codcli
  JOIN productos p1 ON c1.codpro = p1.codpro;

#3- Nombre de los clientes de Santander y nombre de los productos que han comprado

SELECT c.nombre nombrecliente,p.nombre nombreproducto
  FROM poblacion p1  
  JOIN clientes c ON p1.idpob = c.poblacion
  JOIN compran c1 ON c.codcli = c1.codcli
  JOIN productos p ON c1.codpro = p.codpro
  WHERE LEFT(p1.nombre,9)='Santander';
 
#4- Nombre de los clientes de Santander que hayan comprado algo

-- consulta directa
SELECT c.nombre 
  FROM poblacion p1  
  JOIN clientes c ON p1.idpob = c.poblacion
  JOIN compran c1 ON c.codcli = c1.codcli
  WHERE LEFT(p1.nombre,9)='Santander';

-- consulta optimizada

#5- Nombre de los productos que hayan sido comprados por clientes de Santander

-- consulta directa
SELECT DISTINCT p1.nombre
  FROM poblacion p
  JOIN clientes c ON p.idpob = c.poblacion
  JOIN compran c1 ON c.codcli = c1.codcli
  JOIN productos p1 ON c1.codpro = p1.codpro
  WHERE LEFT(p.nombre,9)='Santander';

-- consulta optimizada
  SELECT *
    FROM 
    (SELECT * FROM poblacion p WHERE p.nombre='Santander')AS c1
   

-- Consultas de combinacion externas
#1- Id de las poblaciones que no tengan clientes viviendo
#2- Nombre y apellidos de los clientes que no hayan comprado nada
#3- Nombre de los productos que no hayan sido vendidos.
#4- Nombre de los productos que no hayan sido comprados por clientes de Santander

