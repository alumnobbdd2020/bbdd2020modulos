/*
Mois�s Sarabia Ter�n
Adm�n de bases de datos
Pr�ctica14 m�dulo3 unuidad2
*/

#ejercicio1

DROP DATABASE IF EXISTS p14m3u2;
CREATE DATABASE p14m3u2;
USE p14m3u2;

CREATE TABLE autor(
  dni char(10),
  nombre varchar(50),
  universidad varchar(50),
  PRIMARY KEY(dni)
);

CREATE TABLE tema(
  codigotema int AUTO_INCREMENT NOT NULL,
  descripcion text,
  PRIMARY KEY(codigotema)
);

CREATE TABLE revista(
  referencia varchar(20) NOT NULL,
  titulo_rev varchar(100),
  editorial varchar(50),
  PRIMARY KEY(referencia)
);

CREATE TABLE articulo(
  referencia varchar(20) NOT NULL,
  dni char(10) NOT NULL,
  codigotema int NOT NULL,
  titulo_art varchar(100),
  anno int,
  volumen int,
  numero int,
  paginas int,
  PRIMARY KEY(referencia,dni,codigotema)
);

#ejercicio2

ALTER TABLE articulo
  ADD CONSTRAINT fkrevistadni FOREIGN KEY(dni)
    REFERENCES autor(dni) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT fkrevistatema FOREIGN KEY(codigotema)
    REFERENCES tema(codigotema) ON UPDATE CASCADE ON DELETE CASCADE;