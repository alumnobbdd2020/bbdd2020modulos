/*
Mois�s Sarabia Ter�n
Adm�n de bases de datos
ejemplo de framework
*/

DROP DATABASE IF EXISTS framework_20_11;
CREATE DATABASE framework_20_11;

USE framework_20_11;

CREATE TABLE coches(
  id int NOT NULL,
  marca varchar(50),
  codcliente int,
  fecha date,
  precio float,
  PRIMARY KEY(id)
);

CREATE TABLE clientes(
  cod int NOT NULL,
  nombre varchar(50),
  PRIMARY KEY(cod)
);

ALTER TABLE coches
  ADD CONSTRAINT fkcochescli FOREIGN KEY (codcliente)
   REFERENCES clientes(cod) ON DELETE CASCADE ON UPDATE CASCADE;