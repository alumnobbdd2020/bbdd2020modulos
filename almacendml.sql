--1.) Visualizar los pedidos que hallan excedido las 2500 pts.
select * 
from productos
where precio>2500;
--2.) Visualizar los pedidos que se hallan realizado en el período navideño (suponerlo del 24-12 al 7-1)
select * 
	from productos
	where (extract(month from fechaentrada)=12 and extract(day from fechaentrada)>23)
		or (extract(month from fechaentrada)=1 and extract(day from fechaentrada)<8)
;


--3.) Ver los pedidos realizados a empresas que no tengan sucursal en España

   
--4.) Ver los pedidos cuyo precio por unidad estén entre 50 y 300 pts.
select * 
from productos
where precio between 50 and 300;
--5.) Ver los pedidos cuyo nombre del proveedor empiece por la letra "O"
select *
from productos
where proveedor like 'O%';
--6.) Ver una consulta donde me visualice los pedidos que he realizado a cada proveedor
select count(*),proveedor
from productos
group by proveedor;
--7.) Indicar lo que me gastado en cada producto, es decir, cuanto por Coca-Cola, LecheBrick, etc.
select nombreproducto,sum(cantidades*precio) as gastoporproducto
from productos
group by nombreproducto;
--CONSULTA DE CREACIÓN DE TABLA
/*10.) Crear una tabla nueva donde se visualice el nombre del producto, el nombre del
proveedor, la fecha de pedido y el día de la semana que se pidió.
La tabla se llamará Por días*/
create table pordias as(
	select nombreproducto,proveedor,fechaentrada,extract(dow from fechaentrada)
	from productos
);

/*11.) Crear una tabla donde visualice los productos de España. Los campos a insertar
serán: Nombre del producto, Nombre del proveedor y fecha. La tabla se llamará
Pedidos*/
create table pedidos as(
	select nombreproducto,prov.nombreproveedor,fechaentrada
	from productos as prod
	join proveedores as prov on
	prod.proveedor=prov.codigoproveedor
	where prov.paisprocedencia='España'
);

--CONSULTA DE ACTUALIZACIÓN.
/*12.) Todos los precios de los productos con proveedores con sucursales en España,
bajan un 10% su precio.*/
update productos
set precio=precio-(precio*0.1)
 where proveedor in
 (select prod.proveedor
 from productos as prod
 join proveedores
 on codigoproveedor=prod.proveedor
 where paisprocedencia='España');
--13.) Los proveedores con sucursales en España dejan de tenerla y viceversa.
update proveedores
set suscursalespaña = not true;

--14.) Los productos Leche Brick se denominan ahora Leche encartonada
update productos
set nombreproducto='leche encartonada'
where nombreproducto='LECHE BRICK';

/*CONSULTA DE ELIMINACIÓN
(DISEÑARLAS, NO EJECUTARLAS)*/
--15.) Eliminar los pedidos cuyos productos precios por unidad excedan de 700 pts.
delete 
from productos
where precio>700;
--16.) Eliminar los pedidos con fecha anterior al 31-12-1998
delete 
from productos
where fechaentrada<'31-12-1998';