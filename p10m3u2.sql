/*Mois�s Sarabia Ter�n
  pr�ctica10modulo33unidad2*/

#ejercicio2
DROP DATABASE IF EXISTS p10m3u2;
CREATE DATABASE p10m3u2;
USE p10m3u2;

#ejercicio3
CREATE TABLE empleados(
  dni varchar(20),
  nombre varchar(50),
  edad int,
  estadocivil char(10),
  sueldo float
);

#ejercicio4
CREATE TABLE empresa(
  dni varchar(10),
  dia int,
  horas bit(8),
  ref char(10)
);

#ejercicio5
CREATE TABLE socios(
  dni char(10),
  nombre varchar(50),
  edad int,
  sueldo float,
  fechadenacimiento date,
  PRIMARY KEY(dni)
);

CREATE TABLE sucursales(
  dni char(10),
  referencia varchar(50),
  horas int,
  PRIMARY KEY(referencia)
);

#ejercicio6

INSERT empleados (dni, nombre, edad, estadocivil, sueldo)
  VALUES ('111111111K', 'Luis Garcia', 42, 'casado', 1220),
         ('222222222L', 'Pedro Garcia', 52, 'soltero', 998);

INSERT empresa (dni, dia, horas, ref)
  VALUES ('333333333G', 480, 47, 'ABC001DF03'),
         ('444444444H', 680, 21, 'ABC001DF07');

INSERT socios (dni, nombre, edad, sueldo, fechadenacimiento)
  VALUES ('555555555B', 'Antonio Martinez', 32, 943,'1988/03/14'),
         ('666666666C', 'Maria Saiz', 30, 1091,'1990/06/17');
  
INSERT sucursales (dni, referencia, horas)
  VALUES ('123456789J', 'CANSAN001', 3780),
         ('123456799N', 'CANSAN005', 1780) ;