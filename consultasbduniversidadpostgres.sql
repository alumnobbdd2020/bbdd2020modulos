﻿--1.	Obtener el nombre y la edad de los alumnos matriculados en ‘Secretary’
SELECT
  a.nombre_al,
  a.edad_al
FROM alumnos a
  JOIN estudian e
    ON a.dni_al = e.dni_al
  JOIN asignaturas a1
    ON e.cod_as = a1.cod_as
WHERE a1.nombre_as = 'Secretary';
--2.	Mostrar las edades de todos los alumnos
SELECT
  a.edad_al
FROM alumnos a;
--3.	Mostrar los Alumnos cuyas edades estén comprendidas entre 18 y 25 años
SELECT
  a.dni_al,
  a.nombre_al,
  a.apellido1_al,
  a.apellido2_al,
  a.edad_al,
  a.telefono_al,
  a.direccion_al,
  a.ciudad_al
FROM alumnos a
WHERE a.edad_al BETWEEN 18 AND 25;

--4.	Mostrar nombre y teléfono de los profesores cuyo primer apellido comience por “P”.

SELECT
  p.nombre_p,
  p.telefono_p
FROM profesores p
WHERE p.apellido1_p LIKE 'P%';

--6.	Alumnos con la máxima nota en alguna asignatura.

SELECT
  MAX(e.nota_al_as),
  a.nombre_as
FROM estudian e
  JOIN asignaturas a
    ON e.cod_as = a.cod_as
GROUP BY a.nombre_as;

--7.	Alumnos que residen en la misma ciudad que “Antonia Harper”

SELECT
  *
FROM alumnos a
WHERE a.ciudad_al = (SELECT
    a.ciudad_al
  FROM alumnos a
  WHERE a.nombre_al = 'Antonia'
  AND a.apellido1_al = 'Harper');

--8.	Nombre y apellido de alumnos ordenados de forma descendente por edad

SELECT
  a.nombre_al,
  a.apellido1_al,
  a.apellido2_al,
  a.edad_al
FROM alumnos a
ORDER BY a.edad_al DESC;

--9.	Alumnos matriculados en más de 3 asignaturas
 

--10.	Ha habido un error en el nombre del profesor con el DNI número 58345134P, se llama Arban y no Urban. Actualízalo para que así conste.
  
  UPDATE profesores 
    SET nombre_p='Arban'
  WHERE nombre_p='Urban';

--11.	Elimina los profesores cuyo primer apellido sea “erl”

DELETE FROM profesores
  WHERE apellido1_p='erl';

--12.	Mostrar las notas que tiene cada alumno y la asignatura a la que corresponde la misma (pero solo los alumnos que cursen más de dos asignaturas)

SELECT estudian.nota_al_as,asignaturas.nombre_as
FROM estudian
INNER JOIN asignaturas ON asignaturas.cod_as=estudian.cod_as
WHERE nota_al_as IS NOT NULL;

--13.	Mostrar la nota media, mínima y máxima que ha puesto cada profesor para cada una de las asignaturas que imparte

 SELECT a.nombre_as asignatura, p.nombre_p profe, 
  AVG( e.nota_al_as) media, MIN( e.nota_al_as), MAX( e.nota_al_as) 
  FROM profesores p 
  JOIN asignaturas a ON a.dni_p= p.dni_p 
  JOIN estudian e ON a.cod_as = e.cod_as 
GROUP BY p.dni_p, a.nombre_as;


--14.	Mostrar un listado de alumnos y sus teléfonos ordenados alfabéticamente

SELECT a.nombre_al,a.apellido1_al,a.apellido2_al,a.telefono_al 
  FROM alumnos a
ORDER BY a.apellido1_al;

--15.	Mostrar un listado de alumnos ordenados por su edad, de mayor a menor y, en caso de igualdad, alfabéticamente.

SELECT * 
FROM alumnos a
ORDER BY a.edad_al,a.apellido1_al;

--16.	Mostrar los alumnos que no han sacado una nota entre 7 y 9.

SELECT * FROM alumnos 
 JOIN estudian ON estudian.dni_al=alumnos.dni_al
  WHERE estudian.nota_al_as NOT BETWEEN 7 AND 9; 

--17.	Mostrar los alumnos que han aprobado alguna asignatura

SELECT * FROM alumnos 
 JOIN estudian ON estudian.dni_al=alumnos.dni_al
  WHERE estudian.nota_al_as >5; 

--18.	Mostrar la nota media del alumno apellidado “Doe”

SELECT AVG(nota_al_as) 
FROM estudian 
JOIN alumnos a ON a.dni_al=estudian.dni_al
WHERE a.apellido1_al='Doe'
GROUP BY a.nombre_al;

--19.	Mostrar, de la asignatura secretary, la nota máxima, mínima, y la diferencia entre ambas. Devolver también el número de alumnos que la han cursado.

SELECT COUNT(dni_al) numerodealumnos,AVG(nota_al_as) notamedia,Max(nota_al_as) notamaxima,Min(nota_al_as) notaminima,MAX(nota_al_as)-MIN(nota_al_as) dif
FROM estudian e
JOIN asignaturas a ON e.cod_as=a.cod_as
WHERE a.nombre_as='Secretary';

--20.	Devolver la fecha de nacimiento del alumno mayor.

--21.	Mostrar la Nota_Al_As media de cada alumno.

SELECT dni_al,AVG(e.nota_al_as) notamediaporalumno
  FROM estudian e
  GROUP BY e.dni_al;
