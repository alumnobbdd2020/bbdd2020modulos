--
-- Set default database
--
USE practica8m3u2;

--
-- Create foreign key
--
ALTER TABLE modulo 
  ADD CONSTRAINT `FK_modulo_cursa_codigo-modulo` FOREIGN KEY (codigo)
    REFERENCES cursa(`codigo-modulo`) ON DELETE NO ACTION ON UPDATE NO ACTION;