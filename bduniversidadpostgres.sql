﻿DROP DATABASE IF EXISTS universidad; 
CREATE DATABASE Universidad;

CREATE TABLE alumnos(
  dni_al CHAR(9),
  nombre_al CHARACTER VARYING(50) NOT NULL,
  apellido1_al CHARACTER VARYING(50) NOT NULL,
  apellido2_al CHARACTER VARYING(50) NOT NULL,
  edad_al INT,
  telefono_al CHAR(9),
  direccion_al CHARACTER VARYING(100),
  ciudad_al CHARACTER VARYING(20),
  PRIMARY KEY(dni_al)
);

CREATE TABLE profesores(
  dni_p CHAR(9),
  nombre_p CHARACTER VARYING(50) NOT NULL,
  Apellido1_p CHARACTER VARYING(50) NOT NULL,
  apellido2_p CHARACTER VARYING(50) NOT NULL,
  ncuenta_p CHAR(20),
  telefono_p CHAR(9),
  especialidad_p CHARACTER VARYING(20),
  direccion_p CHARACTER VARYING(100),
  ciudad_p CHARACTER VARYING(20),
  PRIMARY KEY(dni_p)
);

CREATE TABLE asignaturas(
  cod_as INT,
  nombre_as CHARACTER VARYING(50) NOT NULL,
  creditos_as INT NOT NULL,
  facultad_as CHARACTER VARYING(50),
  dni_p CHARACTER(9),
  PRIMARY KEY(cod_as)
);

CREATE TABLE estudian(
  dni_al CHARACTER(9) NOT NULL,
  cod_as INT NOT NULL,
  nota_al_as INT,
  PRIMARY KEY (dni_al,cod_as)
);

ALTER TABLE asignaturas
  ADD CONSTRAINT fkasigpro FOREIGN KEY(dni_p)
   REFERENCES profesores(dni_p) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE estudian
  ADD CONSTRAINT fkestualum FOREIGN KEY(dni_al)
    REFERENCES alumnos(dni_al) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT fkestuasig FOREIGN KEY(cod_as)
    REFERENCES asignaturas(cod_as) ON DELETE CASCADE ON UPDATE CASCADE;
