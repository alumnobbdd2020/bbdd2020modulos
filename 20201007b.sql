/*
  ejemplo del uso del LMD
*/
DROP DATABASE IF EXISTS ejemplo07; #Elimina la BBDD
CREATE DATABASE ejemplo07;
USE ejemplo07; -- selecciona la BBDD
CREATE TABLE personas (
  #definicion de campos
  persona_id int AUTO_INCREMENT,
  persona_nom varchar(50) NOT NULL,
  ciudad_id int,
  correo varchar(50),
  #definicion de indices
  PRIMARY KEY(persona_id),
  KEY indice1(persona_id), -- indexado con duplicados
  UNIQUE uk1(correo) -- indexado sin duplicados
);

CREATE TABLE ciudades (
  #definicion de campos
  ciudad_id int AUTO_INCREMENT,
  ciudad_nom varchar(50) DEFAULT NULL,
  provincia varchar(50) DEFAULT 'Cantabria' NOT NULL,
  #definicion de indices
  PRIMARY KEY(ciudad_id),
  INDEX indice1(ciudad_nom,provincia)
);

INSERT personas (persona_id, persona_nom, ciudad_id)
  VALUES (1, 'Jose', 10),
  (2, 'Ana', 9);

INSERT ciudades (ciudad_nom, provincia)
  VALUES ('Santander', 'Cantabria'),
  ('potes', 'Cantabria'),
  ('laredo', DEFAULT),
  ('Reinosa',DEFAULT);

INSERT ciudades (ciudad_nom)
  VALUES ('Torrelavega'); -- Al no introducir nada en provincia asigna el valor por defecto 

SELECT
  p.persona_id,
  p.persona_nom,
  p.ciudad_id
FROM personas p;

SELECT
  *
FROM ciudades c;