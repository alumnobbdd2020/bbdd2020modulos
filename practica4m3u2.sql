-- MySQL Script generated by MySQL Workbench
-- Fri Sep 25 17:13:05 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`poblacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`poblacion` ;

CREATE TABLE IF NOT EXISTS `mydb`.`poblacion` (
  `codpob` VARCHAR(10) NOT NULL,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`codpob`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`clientes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`clientes` ;

CREATE TABLE IF NOT EXISTS `mydb`.`clientes` (
  `codigo` VARCHAR(10) NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `codpob` VARCHAR(10) NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_clientes_poblacion_idx` (`codpob` ASC),
  CONSTRAINT `fk_clientes_poblacion`
    FOREIGN KEY (`codpob`)
    REFERENCES `mydb`.`poblacion` (`codpob`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
