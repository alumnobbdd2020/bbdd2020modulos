USE ejemplo20201109;

-- Nombre del cliente junto con el nombre de la población donde vive

SELECT c.nombre nombrecliente,p.nombre nombrepoblacion
  FROM clientes c JOIN poblacion p 
  ON c.poblacion = p.idpob;

#2- Nombre de las poblaciones que tengan clientes viviendo
SELECT DISTINCT p.nombre 
FROM poblacion p
  JOIN clientes c ON p.idpob = c.poblacion;

-- producto cartesiano (en caso de no disponer de operador join)

SELECT DISTINCT p.nombre 
FROM poblacion p,clientes c
  WHERE c.poblacion=p.idpob;

#3- Nombre de las poblaciones que tengan clientes que hayan comprado algo

SELECT DISTINCT p.nombre
  FROM poblacion p
  JOIN clientes c ON p.idpob = c.poblacion 
  JOIN compran c1 ON c.codcli = c1.codcli;

  

#4- Nombre de los clientes de Santander
#5- Nombre de los clientes de Santander que hayan comprado algo
  SELECT p.nombre 
    FROM poblacion p
    JOIN clientes c ON p.idpob = c.poblacion 
    JOIN compran c1 ON c.codcli = c1.codcli 
    WHERE p.nombre='Santander';

#6- Nombre de los productos que hayan sido comprados por clientes de Santander

-- Consultas de combinacion externas
#1- Id de las poblaciones que no tengan clientes viviendo
#2- Nombre y apellidos de los clientes que no hayan comprado nada
#3- Nombre de los productos que no hayan sido vendidos.
#4- Nombre de los productos que no hayan sido comprados por clientes de Santander
