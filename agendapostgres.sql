create table contactos(
	numerocontacto serial,
	nombre character varying(20),
	apellidos character varying(40),
	fechanacimiento date,
	telefono character(9) not null default 000000000                    
);

create table clientes(
	codigocliente serial,
	nombre character varying(20),
	apellidos character varying(40),
	dni character(10),
	descuento int default 5,
	tarjetasocio boolean
);

insert into contactos(nombre,apellidos,fechanacimiento,telefono)
values  ('Antonio','García Pérez','15/08/1960',942369521),
		('Carlos','Pérez Ruiz','26/04/1958',942245147),
		('Luis','Rodriguez Mas','30/03/1961',942296578),
		('Jaime','Juangrán Sornes','31/01/1968',942368496),
		('Alfonso','Prats Montolla','28/04/1969',942354852),
		('Jose','Navarro Lard','15/05/1964',942387469),
		('Elisa','Úbeda Sansón','10/07/1962',942357812),
		('Eva','San Martín','12/08/1965',942241589),
		('Juan','García Gómez','15/05/1974',942359887);
		
insert into clientes(nombre,apellidos,dni,descuento,tarjetasocio)
values	('Gerardo','Hernandez Luis','11111111A',5,true),
		('Carlos','Prats Ruiz','22222222B',5,false),
		('Lourdes','Oliver Peris','33333333C',10,true),
		('Sergio','Larred Navas','44444444D',15,true),
		('Joaquín','Árboles Onsins','55555555E',5,false),
		('Antonio','Aznar Zapatero','66666666F',10,true);